import React, { Component } from 'react';
import Navbar from "./components/Navbar";
import CreateUser from "./components/user/CreateUser";
import UserTable from './components/user/UserTable';
import UserForm from "./components/user/UserForm";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Navbar/>
            <div className="container">
              <Switch>
                <Route path='/home' component={UserTable}/>
                <Route path='/user' component={CreateUser} />
                <Route path='/user/:userId' component={UserForm}/>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}
