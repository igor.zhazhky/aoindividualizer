import React, { PureComponent} from 'react';
import PropTypes from 'prop-types';

export default class TextInput extends PureComponent {

  render() {
    return (
    <div className={this.props.className}>
      <label htmlFor={this.props.label}>{this.props.label}</label>
      <input 
        type="text" 
        className="form-control"
        id={this.props.id} 
        name={this.props.name}/>
        <div className="invalid-feedback">
          Valid {this.props.label} is required.
        </div>
    </div>
    );
  }
}

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string
};

TextInput.defaultProps = {
  className: "col-md-6 mb-3"
}