import React, { Component } from 'react';
import './Navbar.css';
import { Link } from 'react-router-dom';


export default class Navbar extends Component {

  render() {
        return (
          <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
            <h5 className="my-0 mr-md-auto font-weight-normal"><Link  to={'/home'}>Asphodel</Link></h5>
            
            <nav className="my-2 my-md-0 mr-md-3">
                <Link className="p-2 text-dark" to={'/home'}>Home</Link>
                <Link className="p-2 text-dark" to={'/user'}>New user</Link>
            </nav>
              <li className="btn btn-outline-primary">Log out</li>
          </div>
        );
    }
}