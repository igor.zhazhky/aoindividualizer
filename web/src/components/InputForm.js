import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './InputForm.css';

export default class InputForm extends Component {
  static PropTypes = {
    children: PropTypes.node.isRequired
  };



  render() {
        return (
          <div className="container">
            <form>
              {this.props.children}
              <div className="row col-md-12">
                <div className="col-2">
                  <button className="btn col-12 btn-success">Save</button>
                </div>
                <div className="col-2">
                  <button className="btn col-12 btn-danger">Cancel</button>             
                </div>
              </div>
            </form>
          </div>
        );
  }
}