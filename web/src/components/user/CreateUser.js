import React, { Component } from 'react';
import PropTypes from 'prop-types'
import TextInput from "../TextInput";
import formSerialize from 'form-serialize';
import "./CreateUser.css";

const URL = 'http://localhost:8090/users';

export default class CreateUser extends Component {

  static propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
  };

  submitForm = (event) => {
    event.preventDefault();
    var data = formSerialize(event.target, { hash: true });
    fetch(URL, {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
      }).then(() => {
        this.props.history.push('/user/1');
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <form onSubmit={this.submitForm}>
      <h2>New user creation</h2>
      <div className="alert alert-info" role="alert">
        All fields are required!
      </div>
        <div className="form-row">
          <TextInput
            label="First name"
            name="name"
            id="firstName"/>
          <TextInput
            label="Second name"
            name="surname"
            id="surname"/>       
        </div>

        <div className="form-row">
          <TextInput
            label="Age"
            name="age"
            id="age"/>
          <TextInput
            label="historyNumber"
            name="historyNumber"
            id="historyNumber"/>       
        </div>

        <div className="row col-md-12">
          <div className="col-2">
            <button type="submit" className="btn col-12 btn-success">Save</button>
          </div>
          <div className="col-2">
            <button className="btn col-12 btn-danger">Cancel</button>             
          </div>
        </div>
      </form>        
    );
  }
}