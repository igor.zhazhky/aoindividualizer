import React, { Component } from 'react';
import './UserForm.css';

const URL = 'http://localhost:8090/users/';

export default class UserForm extends Component {

    state = {
        user: null
    }

    componentDidMount () {
        const { patientId } = this.props.match.params;

        fetch(URL + patientId)
        .then((response) => response.json())
        .then((receivedPatient) => {      
            if (receivedPatient) {
                this.setState({user : receivedPatient})
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {
        return (
        <div>
           <div className="card z-depth-3">
                <div className="card-body">
                <div className="tab-content p-3">
                    <div className="tab-pane active show" id="profile">
                        <h5 className="mb-3">User Profile</h5>
                        <div className="row">
                            <div className="col-md-6">
                                <h6>About</h6>
                                <p>
                                    
                                </p>
                                <h6>Hobbies</h6>
                                <p>
                                    Indie music, skiing and hiking. I love the great outdoors.
                                </p>
                            </div>
                            <div className="col-md-6">
                                <h6>Recent badges</h6>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">html5</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">react</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">codeply</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">angularjs</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">css3</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">jquery</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">bootstrap</a>
                                <a href="javascript:void();" className="badge badge-dark badge-pill">responsive-design</a>
                                <hr/>
                                <span className="badge badge-primary"><i className="fa fa-user"></i> 900 Followers</span>
                                <span className="badge badge-success"><i className="fa fa-cog"></i> 43 Forks</span>
                                <span className="badge badge-danger"><i className="fa fa-eye"></i> 245 Views</span>
                            </div>
                            <div className="col-md-12">
                                <h5 className="mt-2 mb-3"><span className="fa fa-clock-o ion-clock float-right"></span> Recent Activity</h5>
                                <table className="table table-hover table-striped">
                                    <tbody>                                    
                                        <tr>
                                            <td>
                                                <strong>Abby</strong> joined ACME Project Team in <strong>`Collaboration`</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Gary</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Kensington</strong> deleted MyBoard3 in <strong>`Discussions`</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>John</strong> deleted My Board1 in <strong>`Discussions`</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Skell</strong> deleted his post Look at Why this is.. in <strong>`Discussions`</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane" id="messages">
                        <div className="alert alert-info alert-dismissible" role="alert">
                    <button type="button" className="close" data-dismiss="alert">×</button>
                        <div className="alert-icon">
                        <i className="icon-info"></i>
                        </div>
                        <div className="alert-message">
                        <span><strong>Info!</strong> Lorem Ipsum is simply dummy text.</span>
                        </div>
                    </div>
                        <table className="table table-hover table-striped">
                            <tbody>                                    
                                <tr>
                                    <td>
                                    <span className="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span className="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span className="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span className="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <span className="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
                                    </td>
                                </tr>
                            </tbody> 
                        </table>
                    </div>
                    <div className="tab-pane" id="edit">
                        <form>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">First name</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="text" value="Mark"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Last name</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="text" value="Jhonsan"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Email</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="email" value="mark@example.com"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Change profile</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="file"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Website</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="url" value=""/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Address</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="text" value="" placeholder="Street"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label"></label>
                                <div className="col-lg-6">
                                    <input className="form-control" type="text" value="" placeholder="City"/>
                                </div>
                                <div className="col-lg-3">
                                    <input className="form-control" type="text" value="" placeholder="State"/>
                                </div>
                            </div>
                        
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Username</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="text" value="jhonsanmark"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Password</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="password" value="11111122333"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                <div className="col-lg-9">
                                    <input className="form-control" type="password" value="11111122333"/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-lg-3 col-form-label form-control-label"></label>
                                <div className="col-lg-9">
                                    <input type="reset" className="btn btn-secondary" value="Cancel"/>
                                    <input type="button" className="btn btn-primary" value="Save Changes"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}