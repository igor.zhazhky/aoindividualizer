import React, { Component } from 'react';

import './UserTable.css';

const URL = 'http://localhost:8090/users';

export default class CreateUser extends Component {
    
    constructor(props) {
      super(props);

      this.state = {
        users: [],
      };
    }

    componentDidMount() {
      this.loadUsers();
    }

    loadUsers = () => {
        fetch(URL, {
            method: 'GET',
            headers: {
                'Accept': 'application/json'              
            },
          })
          .then((response) => response.json())
          .then((receivedUsers) => {
            if (receivedUsers && receivedUsers.length !== 0) {
              this.setState({users: receivedUsers})
            }
            return receivedUsers;
          })
          .catch((error) => {
            console.error(error);
          });
      }

    render() {
      if (this.state.users.length === 0) {
          return (
            <div>No-no-no</div>
          )
      } else {
          return (
            <div>
            <table className="table table-hover">
                <thead>
                    <tr>
                      <th scope="col">HistoryNumber</th>
                      <th scope="col">Name</th>
                      <th scope="col">Surname</th>
                      <th scope="col">Age</th>
                      <th scope="col">Sex</th>
                      <th scope="col">Disease</th>
                      <th scope="col">Department</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.users.map(user =>
                  <tr>
                      <th scope="row">{user.historyNumber}</th>
                      <td>{user.name}</td>
                      <td>{user.surname}</td>
                      <td>{user.age}</td>
                      <td>{user.sex}</td>
                      <td>{user.disease}</td>
                      <td>{user.department}</td>
                  </tr>
                  )}
                </tbody>            
            </table>
            </div>
        );
      }         
    }
}