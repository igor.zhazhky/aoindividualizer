package com.nure.individualizer.core.exception.sensor;

import com.nure.individualizer.core.exception.ServiceException;

public class DataAlreadySubmittedException extends ServiceException {

    public DataAlreadySubmittedException() {
        super();
    }
}
