package com.nure.individualizer.core.exception.user;

import com.nure.individualizer.core.exception.ServiceException;

public class NoSuchUserException extends ServiceException {

    public NoSuchUserException() {
        super();
    }
}
