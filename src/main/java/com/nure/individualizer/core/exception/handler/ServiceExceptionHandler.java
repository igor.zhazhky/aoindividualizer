package com.nure.individualizer.core.exception.handler;

import com.nure.individualizer.core.exception.ExceptionResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<ExceptionResponseBody> RuntimeExceptionHandler(RuntimeException exception) {
        ExceptionDescriber exceptionDescriber = ExceptionDescriber.getExceptionDescriber(exception.getClass());
        ExceptionResponseBody response = new ExceptionResponseBody(
                exceptionDescriber, exception.getMessage());
        return new ResponseEntity<>(response, exceptionDescriber.getHttpStatus());
    }
}
