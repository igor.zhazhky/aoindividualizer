package com.nure.individualizer.core.exception.sensor;

import com.nure.individualizer.core.exception.ServiceException;

public class NoSuchDataException extends ServiceException {

    public NoSuchDataException() {
        super();
    }
}
