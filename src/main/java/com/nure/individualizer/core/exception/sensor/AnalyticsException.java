package com.nure.individualizer.core.exception.sensor;

import com.nure.individualizer.core.exception.ServiceException;

public class AnalyticsException extends ServiceException {

    public AnalyticsException() {
        super();
    }
}
