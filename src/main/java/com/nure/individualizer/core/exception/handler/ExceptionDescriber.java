package com.nure.individualizer.core.exception.handler;

import com.nure.individualizer.core.exception.sensor.AnalyticsException;
import com.nure.individualizer.core.exception.sensor.DataAlreadySubmittedException;
import com.nure.individualizer.core.exception.sensor.NoSuchDataException;
import com.nure.individualizer.core.exception.user.NoSuchUserException;
import org.springframework.http.HttpStatus;

/**
 * Contains constants for main server exceptions.
 */
public enum  ExceptionDescriber {

    NO_SUCH_USER(NoSuchUserException.class, 4004, "Such user does not exist.",
            HttpStatus.NOT_FOUND),
    USER_ALREADY_EXISTS(NoSuchUserException.class, 4004, "Such user already exists.",
            HttpStatus.CONFLICT),
    NO_SUCH_SENSOR_DATA(NoSuchDataException.class, 4004, "Such data block was not submitted.",
            HttpStatus.NOT_FOUND),
    DATA_ALREADY_EXISTS(DataAlreadySubmittedException.class, 4004, "Such block was already submitted.",
            HttpStatus.CONFLICT),
    ANALYTICS_EXCEPTION(AnalyticsException.class, 4004, "Data analysis exception.",
            HttpStatus.CONFLICT),
    INTERNAL_ERROR(Exception.class, 5000, "Internal error has been occurred.",
            HttpStatus.INTERNAL_SERVER_ERROR);

    private Class exception;
    private int errorCode;
    private String error;
    private HttpStatus httpStatus;

    ExceptionDescriber(Class exception, int errorCode, String error, HttpStatus httpStatus) {
        this.exception = exception;
        this.errorCode = errorCode;
        this.error = error;
        this.httpStatus = httpStatus;
    }

    public Class getException() {
        return exception;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getError() {
        return error;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
    public static ExceptionDescriber getExceptionDescriber(Class intercepted) {
        for (ExceptionDescriber element : values()) {
            if (element.exception.isAssignableFrom(intercepted)) {
                return element;
            }
        }
        return ExceptionDescriber.INTERNAL_ERROR;
    }
}
