package com.nure.individualizer.core.exception;

import java.time.LocalDateTime;

import com.google.common.base.Objects;
import com.nure.individualizer.core.exception.handler.ExceptionDescriber;

public class ExceptionResponseBody {

    private LocalDateTime timestamp;
    private int statusCode;
    private String errorMsg;
    private String description;

    public ExceptionResponseBody(ExceptionDescriber describer, String exceptionMessage) {
        timestamp = LocalDateTime.now();
        this.statusCode = describer.getErrorCode();
        this.errorMsg = describer.getError();
        this.description = exceptionMessage;
    }

    public ExceptionResponseBody(int statusCode, String errorMsg, String description) {
        this.timestamp = LocalDateTime.now();
        this.statusCode = statusCode;
        this.errorMsg = errorMsg;
        this.description = description;
    }

    public ExceptionResponseBody(int statusCode, String errorMsg) {
        this.statusCode = statusCode;
        this.errorMsg = errorMsg;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExceptionResponseBody that = (ExceptionResponseBody) o;
        return statusCode == that.statusCode &&
                Objects.equal(timestamp, that.timestamp) &&
                Objects.equal(errorMsg, that.errorMsg) &&
                Objects.equal(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(timestamp, statusCode, errorMsg, description);
    }
}
