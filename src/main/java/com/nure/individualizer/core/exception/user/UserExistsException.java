package com.nure.individualizer.core.exception.user;

import com.nure.individualizer.core.exception.ServiceException;

public class UserExistsException extends ServiceException {

    public UserExistsException() {
        super();
    }
}
