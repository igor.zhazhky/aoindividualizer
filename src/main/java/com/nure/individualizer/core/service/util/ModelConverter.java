package com.nure.individualizer.core.service.util;

public interface ModelConverter<E, D> {

    D convertEntity(E sourceEntity);

    E convertDto(D sourceDto);
}
