package com.nure.individualizer.core.service.util;

import com.nure.individualizer.core.entity.SensorData;
import com.nure.individualizer.core.entity.SensorType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class SensorDataGenerator {

    /**
     * Generate one data block with random info.
     */
    public SensorData generateDataBlock() {
        SensorData sensorData = new SensorData();
        sensorData.setPurpose(getSensorType().toString());
        sensorData.setAmount(getRandomInt(100));
        sensorData.setReceived(getRandomLong(100L, 999L));
        sensorData.setValid(getRandomBoolean());
        return sensorData;
    }

    /**
     * Generate data bundle with random sensor data objects.
     */
    public List<SensorData> generataDataBundle(int numberOfElements) {
        List<SensorData> list = new ArrayList<>();
        for (int i = 0; i <= numberOfElements; i++) {
            list.add(generateDataBlock());
        }
        return list;
    }

    private SensorType getSensorType() {
        return SensorType.values()[getRandomInt(SensorType.values().length)];
    }

    public int getRandomInt(int boundaries) {
        Random random = new Random();
        return random.nextInt(boundaries);
    }

    private long getRandomLong(long leftLimit, long rightLimit) {
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    private boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
