package com.nure.individualizer.core.service;

import com.nure.individualizer.core.entity.SensorData;
import com.nure.individualizer.core.entity.SensorType;
import com.nure.individualizer.core.exception.sensor.NoSuchDataException;
import com.nure.individualizer.core.repository.SensorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SensorService {

    private final SensorDataRepository sensorDataRepository;

    @Autowired
    public SensorService(SensorDataRepository sensorDataRepository) {
        this.sensorDataRepository = sensorDataRepository;
    }

    public SensorData get(long id) {
        return sensorDataRepository.findById(id)
                .orElseThrow(NoSuchDataException::new);
    }

    public List<SensorData> get(SensorType sensorType) {
        return sensorDataRepository.findAllByPurpose(sensorType.toString());
    }

    public List<SensorData> getAllData() {
        return sensorDataRepository.findAll();
    }

    public long submitBlock(SensorData sensorData) {
        sensorData = sensorDataRepository.save(sensorData);
        return sensorData.getId();
    }

    public void submitBundle(List<SensorData> sensorData) {
        sensorDataRepository.saveAll(sensorData);
    }

    public void deleteAll() {
        sensorDataRepository.deleteAll();
    }
}
