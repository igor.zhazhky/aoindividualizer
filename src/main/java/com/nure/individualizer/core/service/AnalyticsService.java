package com.nure.individualizer.core.service;

import com.google.common.base.Stopwatch;
import com.nure.individualizer.core.entity.SensorData;
import com.nure.individualizer.core.service.util.SensorDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class AnalyticsService {

    private final Logger log = Logger.getLogger(AnalyticsService.class.getName());

    private final SensorDataGenerator sensorDataGenerator;

    @Autowired
    public AnalyticsService(SensorDataGenerator sensorDataGenerator) {
        this.sensorDataGenerator = sensorDataGenerator;
    }

    /**
     * Calculate some random long result and set it to SensorData for future DB save.
     */
    public Long calculateResult(SensorData sensorData) throws InterruptedException {
        return ThreadLocalRandom.current().nextLong(10, 100);
    }

    /**
     * Calculate some random long result and set it to SensorData for future DB save.
     *
     * @return list of calculated data.
     */
    public List<SensorData> calculateResultForBundle(List<SensorData> sensorDataList) {
        Stopwatch timer = Stopwatch.createStarted();
        log.info("=========================== Result calculation ========================================");
        log.info("Number of objects: " + sensorDataList.size());

        sensorDataList.forEach(sensorData ->
                        sensorData.setCalculationResult(ThreadLocalRandom.current().nextLong(10, 100)));

        log.info("Time spent   : " + timer.elapsed(TimeUnit.MILLISECONDS) + " milliseconds");
        log.info("Time spent   : " + timer.elapsed(TimeUnit.SECONDS) + " seconds");
        log.info("=========================== End of calculation ========================================");
        timer.stop();
        return sensorDataList;
    }

    /**
     * Filters list of SensorData and keeps only crucial sensors.
     */
    public List<SensorData> filterData(List<SensorData> sensorDataList) {
        Stopwatch timer = Stopwatch.createStarted();
        log.info("=========================== Filtering ========================================");
        log.info("Number of objects: " + sensorDataList.size());
        List<SensorData> filteredList = sensorDataList.stream()
                .filter(SensorData::isValid)
                .collect(Collectors.toList());

        log.info("Time spent   : " + timer.elapsed(TimeUnit.MILLISECONDS) + " milliseconds");
        log.info("Time spent   : " + timer.elapsed(TimeUnit.SECONDS) + " seconds");
        log.info("=========================== End of filtering ========================================");
        timer.stop();
        return filteredList;
    }

    /**
     * Returns most optimal Sensor from the list.
     */
    public SensorData getMostOptimal(List<SensorData> sensorDataList) throws InterruptedException {
        Stopwatch timer = Stopwatch.createStarted();
        log.info("=========================== Search for most optimal ===================================");
        log.info("Number of objects: " + sensorDataList.size());

        int seed = sensorDataList.size() + 1;
        Thread.sleep(ThreadLocalRandom.current().nextLong(seed, seed * 2));

        log.info("Time spent   : " + timer.elapsed(TimeUnit.MILLISECONDS) + " milliseconds");
        log.info("Time spent   : " + timer.elapsed(TimeUnit.SECONDS) + " seconds");
        log.info("=========================== End of search for optimal ====================================");
        timer.stop();
        return sensorDataList.get(sensorDataGenerator.getRandomInt(sensorDataList.size()));
    }

    /**
     * Starts data analysis.
     */
    public void startDataAnalysis(List<SensorData> sensorDataList) {
        Stopwatch timer = Stopwatch.createStarted();
        log.info("=========================== Data analysis ===================================");
        log.info("Number of objects: " + sensorDataList.size());

        List<SensorData> collect = sensorDataList.parallelStream()
                .map(sensor -> {
                    String value = sensor.getPurpose();
                    int size = sensorDataList.size();
                    for (int i = 0; i <= 779; i++) {
                        value = value + ThreadLocalRandom.current().nextInt(size);
                    }
                    return sensor;
                })
                .collect(Collectors.toList());

        log.info("Time spent   : " + timer.elapsed(TimeUnit.MILLISECONDS) + " milliseconds");
        log.info("Time spent   : " + timer.elapsed(TimeUnit.SECONDS) + " seconds");
        log.info("=========================== End of analysis ====================================");
        timer.stop();
    }
}
