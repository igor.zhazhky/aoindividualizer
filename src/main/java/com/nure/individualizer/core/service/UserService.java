package com.nure.individualizer.core.service;

import java.util.List;

import com.nure.individualizer.core.dto.UserDto;

public interface UserService {

    UserDto getUser(long id);

    long createUser(UserDto user);

    List<UserDto> getAllUsers();

}
