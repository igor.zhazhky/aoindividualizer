package com.nure.individualizer.core.service.util;

import java.time.LocalDate;

import com.nure.individualizer.core.dto.UserDto;
import com.nure.individualizer.core.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelConverter implements ModelConverter<User, UserDto> {

    @Autowired
    public UserModelConverter() {
    }

    @Override
    public UserDto convertEntity(User user) {
        UserDto dto = new UserDto();
        BeanUtils.copyProperties(user, dto);
        dto.setHistoryNumber(String.valueOf(user.getHistoryNumber()));
        return dto;
    }

    @Override
    public User convertDto(UserDto dto) {
        User user = new User();
        BeanUtils.copyProperties(dto, user);
        user.setHistoryNumber(Long.parseLong(dto.getHistoryNumber()));
        user.setCreationDate(LocalDate.now());
        return user;
    }

}
