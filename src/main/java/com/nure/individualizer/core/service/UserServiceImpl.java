package com.nure.individualizer.core.service;

import java.util.List;
import java.util.stream.Collectors;

import com.nure.individualizer.core.dto.UserDto;
import com.nure.individualizer.core.entity.User;
import com.nure.individualizer.core.exception.user.NoSuchUserException;
import com.nure.individualizer.core.repository.UserRepository;
import com.nure.individualizer.core.service.util.UserModelConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserModelConverter userConverter;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserModelConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Override
    public UserDto getUser(long id) {
        User user = userRepository.findById(id)
                .orElseThrow(NoSuchUserException::new);
        return userConverter.convertEntity(user);
    }

    @Override
    public long createUser(UserDto user) {
        User userToSave = userConverter.convertDto(user);
        userToSave = userRepository.save(userToSave);
        return userToSave.getId();
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> receivedUsers = (List<User>) userRepository.findAll();
        return receivedUsers.stream().map(userConverter::convertEntity).collect(Collectors.toList());
    }
}
