package com.nure.individualizer.core.entity;

public enum SensorType {
    SOLAR_ENERGY,
    WIND_ENERGY,
    SMART_HOUSE,
    SMART_CAR,
    ANALYZER
}
