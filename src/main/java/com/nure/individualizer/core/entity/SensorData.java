package com.nure.individualizer.core.entity;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SensorData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * Purpose of the sensor.
     */
    private String purpose;

    /**
     * Number of working sensors.
     */
    private int amount;

    /**
     * Some received sensor data.
     */
    private Long received;

    /**
     * Data calculation result.
     */
    private Long calculationResult;

    private boolean isValid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Long getReceived() {
        return received;
    }

    public void setReceived(Long received) {
        this.received = received;
    }

    public Long getCalculationResult() {
        return calculationResult;
    }

    public void setCalculationResult(Long calculationResult) {
        this.calculationResult = calculationResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorData that = (SensorData) o;
        return id == that.id &&
                Objects.equal(purpose, that.purpose) &&
                Objects.equal(amount, that.amount) &&
                Objects.equal(received, that.received) &&
                Objects.equal(calculationResult, that.calculationResult);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, purpose, amount, received, calculationResult);
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
