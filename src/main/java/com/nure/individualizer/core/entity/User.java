package com.nure.individualizer.core.entity;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class User implements Serializable {

    private static final long serialVersionUID = 4865903039190150223L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private String surname;

    private int age;

    private long historyNumber;

    private LocalDate creationDate;

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getHistoryNumber() {
        return historyNumber;
    }

    public void setHistoryNumber(long historyNumber) {
        this.historyNumber = historyNumber;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                age == user.age &&
                historyNumber == user.historyNumber &&
                Objects.equal(name, user.name) &&
                Objects.equal(surname, user.surname) &&
                Objects.equal(creationDate, user.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, surname, age, historyNumber, creationDate);
    }
}
