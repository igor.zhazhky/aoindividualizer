package com.nure.individualizer.core.controller;

import java.util.List;

import com.nure.individualizer.core.dto.UserDto;
import com.nure.individualizer.core.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Autowired
    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDto getUser(long id) {
        return userService.getUser(id);
    }

    @Override
    public long createUser(UserDto user) {
        return userService.createUser(user);
    }

    @Override
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<UserDto> receivedUsers = userService.getAllUsers();
        return ResponseEntity.ok(receivedUsers);
    }
}
