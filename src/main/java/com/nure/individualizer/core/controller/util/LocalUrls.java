package com.nure.individualizer.core.controller.util;

public final class LocalUrls {

    public static final String LOCALHOST_STRING = "http://localhost:5000";

    public static final String SUBMIT_DATA_BUNDLE_URL =
            LOCALHOST_STRING + "/data/bundles";

    public static final String DATA_URL = LOCALHOST_STRING + "/data";

    public static final String GET_OPTIMAL_URL = LOCALHOST_STRING + "/data/optimal";

    public static final String ANALYTICS_URL = LOCALHOST_STRING + "/data/analysis";
}

