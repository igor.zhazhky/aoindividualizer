package com.nure.individualizer.core.controller.util;

public final class AwsUrls {

    public static final String AWS_HOST = "http://cloud-env.any3wz6pnd.us-east-2.elasticbeanstalk.com";

    public static final String SUBMIT_DATA_BUNDLE_URL =
            AWS_HOST + "/data/bundles";

    public static final String DATA_URL = AWS_HOST + "/data";

    public static final String GET_OPTIMAL_URL = AWS_HOST + "/data/optimal";

    public static final String ANALYTICS_URL = AWS_HOST + "/data/analysis";

}

