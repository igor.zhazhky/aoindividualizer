package com.nure.individualizer.core.controller;

import java.util.List;

import com.nure.individualizer.core.entity.SensorData;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface SensorDataController {

    @GetMapping(value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<SensorData>> getAll();

    @PostMapping(value = "/data/bundles", consumes = MediaType.APPLICATION_JSON_VALUE)
    void create(@RequestBody List<SensorData> sensorData);

    @GetMapping(value = "/data/optimal", produces = MediaType.APPLICATION_JSON_VALUE)
    SensorData optimal();

    @GetMapping(value = "/data/analysis")
    void analysis();

    @GetMapping(value = "/data/size", produces = MediaType.APPLICATION_JSON_VALUE)
    Integer size();

    @DeleteMapping(value = "/data/clear")
    void clearAll();
}
