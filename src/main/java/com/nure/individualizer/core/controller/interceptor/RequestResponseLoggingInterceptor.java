package com.nure.individualizer.core.controller.interceptor;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import com.google.common.base.Stopwatch;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    private final Logger log = Logger.getLogger(RequestResponseLoggingInterceptor.class.getName());

    /**
     * Interceptor for request time measures.
     *
     * @param request Http request
     * @param body  request's body
     * @param execution HTTP request execution
     * @return Http response
     * @throws IOException in case of I/O errors
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest request,
                                        byte[] body,
                                        ClientHttpRequestExecution execution) throws IOException {
        Stopwatch timer = Stopwatch.createStarted();
        logRequest(request, body);

        ClientHttpResponse response = execution.execute(request, body);

        logResponse(response, timer);
        timer.stop();

        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) {
        log.info("===========================request begin================================================");
        log.info("URI         : " + request.getURI());
        log.info("Method      : " + request.getMethod());
        log.info("Request body: " + new String(body, StandardCharsets.UTF_8));
        log.info("==========================request end================================================");
    }

    private void logResponse(ClientHttpResponse response, Stopwatch timer) throws IOException {
        log.info("============================response begin==========================================");
        log.info("Status code  : " + response.getStatusCode());
        log.info("Time spent   : " + timer.elapsed(TimeUnit.MILLISECONDS) + " milliseconds");
        log.info("Time spent   : " + timer.elapsed(TimeUnit.SECONDS) + " seconds");
        log.info("Response body: " + StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        log.info("=======================response end=================================================");
    }
}
