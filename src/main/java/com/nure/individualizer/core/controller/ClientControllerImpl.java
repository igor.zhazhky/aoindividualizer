package com.nure.individualizer.core.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nure.individualizer.core.controller.interceptor.RequestResponseLoggingInterceptor;
import com.nure.individualizer.core.controller.util.AwsUrls;
import com.nure.individualizer.core.entity.SensorData;
import com.nure.individualizer.core.service.util.SensorDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ClientControllerImpl implements ClientController {


    private static final int SIZE_OF_BUNDLE = 10000;
    private final SensorDataGenerator sensorDataGenerator;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    public ClientControllerImpl(SensorDataGenerator sensorDataGenerator) {
        this.sensorDataGenerator = sensorDataGenerator;
        restTemplate.setInterceptors(Collections.singletonList(
                new RequestResponseLoggingInterceptor()));
    }

    @Override
    public void sendBundle() throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(objectMapper.writeValueAsString(generateBundle()), headers);
        restTemplate.postForLocation(AwsUrls.SUBMIT_DATA_BUNDLE_URL, request);
    }

    private List<SensorData> generateBundle() {
        return sensorDataGenerator.generataDataBundle(SIZE_OF_BUNDLE);
    }

    @Override
    public ArrayList getAll() {
        return restTemplate.getForObject(AwsUrls.DATA_URL, ArrayList.class);
    }

    @Override
    public SensorData getOptimal() {
        return restTemplate.getForObject(AwsUrls.GET_OPTIMAL_URL, SensorData.class);
    }

    @Override
    public void startAnalytics() {
        restTemplate.getForObject(AwsUrls.ANALYTICS_URL, SensorData.class);
    }
}
