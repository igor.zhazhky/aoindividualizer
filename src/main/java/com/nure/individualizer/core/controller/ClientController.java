package com.nure.individualizer.core.controller;

import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nure.individualizer.core.entity.SensorData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface ClientController {

    @GetMapping(value = "/sensors/bundles")
    void sendBundle() throws JsonProcessingException, InterruptedException;

    @GetMapping(value = "/sensors")
    ArrayList getAll();

    @GetMapping(value = "/sensors/optimal")
    SensorData getOptimal();

    @GetMapping(value = "/sensors/analysis")
    void startAnalytics();
}
