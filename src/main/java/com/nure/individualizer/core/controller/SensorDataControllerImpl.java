package com.nure.individualizer.core.controller;

import java.util.List;

import com.nure.individualizer.core.entity.SensorData;
import com.nure.individualizer.core.exception.sensor.AnalyticsException;
import com.nure.individualizer.core.service.AnalyticsService;
import com.nure.individualizer.core.service.SensorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:5000")
public class SensorDataControllerImpl implements SensorDataController {

    private final SensorService sensorService;
    private final AnalyticsService analyticsService;

    public SensorDataControllerImpl(SensorService sensorService,
                                    AnalyticsService analyticsService) {
        this.sensorService = sensorService;
        this.analyticsService = analyticsService;
    }

    @Override
    public ResponseEntity<List<SensorData>> getAll() {
        return ResponseEntity.ok(sensorService.getAllData());
    }

    @Override
    public void create(List<SensorData> sensorDataList) {
        try {
            sensorDataList = analyticsService.filterData(sensorDataList);
            sensorDataList = analyticsService.calculateResultForBundle(sensorDataList);
            sensorService.submitBundle(sensorDataList);
        } catch (InterruptedException e) {
            throw new AnalyticsException();
        }
    }

    @Override
    public SensorData optimal() {
        try {
            List<SensorData> listOfSensors = sensorService.getAllData();
            return analyticsService.getMostOptimal(listOfSensors);
        } catch (InterruptedException e) {
            throw new AnalyticsException();
        }
    }

    @Override
    public void analysis() {
        try {
            List<SensorData> listOfSensors = sensorService.getAllData();
            analyticsService.startDataAnalysis(listOfSensors);
        } catch (InterruptedException e) {
            throw new AnalyticsException();
        }
    }

    @Override
    public Integer size() {
        return sensorService.getAllData().size();
    }

    @Override
    public void clearAll() {
        sensorService.deleteAll();
    }

}
