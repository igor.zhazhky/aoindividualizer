package com.nure.individualizer.core.controller;

import java.util.List;

import com.nure.individualizer.core.dto.UserDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface UserController {

    @GetMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    UserDto getUser(@PathVariable long id);

    @PostMapping(name = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
    long createUser(@RequestBody UserDto user);

    @GetMapping(name = "/users")
    ResponseEntity<List<UserDto>> getAllUsers();
}
