package com.nure.individualizer.core.dto;

import java.io.Serializable;

public class UserDto implements Serializable {

    private static final long serialVersionUID = 4865903039190150223L;

    private Long id;

    private String name;

    private String surname;

    private int age;

    private String sex;

    private String historyNumber;

    private String disease;

    private String department;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHistoryNumber() {
        return historyNumber;
    }

    public void setHistoryNumber(String historyNumber) {
        this.historyNumber = historyNumber;
    }
}
