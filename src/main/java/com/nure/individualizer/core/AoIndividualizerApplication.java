package com.nure.individualizer.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AoIndividualizerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AoIndividualizerApplication.class, args);
    }

}

