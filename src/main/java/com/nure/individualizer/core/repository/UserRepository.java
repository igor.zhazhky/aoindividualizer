package com.nure.individualizer.core.repository;

import com.nure.individualizer.core.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findByName(String name);

}
