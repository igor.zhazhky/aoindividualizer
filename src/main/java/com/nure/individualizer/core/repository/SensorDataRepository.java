package com.nure.individualizer.core.repository;

import java.util.List;

import com.nure.individualizer.core.entity.SensorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SensorDataRepository extends JpaRepository<SensorData, Long> {

    List<SensorData> findAllByPurpose(String purpose);
}
